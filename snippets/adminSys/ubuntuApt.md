# Apt et gestion de paquets

## Installer des applications

### À partir d'un fichier .deb

Pour ces fichiers, il suffit de les télécharger et de les exécuter.

### Via package manager (apt)

Une fois que l'on connait le nom du paquet, il faut lancer les commandes :

- `sudo apt update` pour mettre à jour le cache de apt (la liste des paquets disponibles et leurs versions)
- `sudo apt install nom_du_programme` pour installer le programme voulu

Si le programme n'est pas dans les repositories de votre distribution, il faudra suivre les instructions du programme et ajouter le repository. Un exemple pour installer nodeJS, version 12 :

```bash
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```

## Désinstaller une application

`sudo apt remove nom_du_programme` désinstalle le programme et conserve sa configuration ou les fichiers enregistrés par le programme.

### Désinstaller et supprimer la configuration

`sudo apt purge nom_du_programme` supprime TOUS les fichiers du programme, configuration incluse.

Certains paquets ajoutent également un repository (serveur dédié pour télécharger les paquets). En général, ils ajoutent un fichier dans `/etc/apt/sources.list.d/` et vous pouvez le supprimer si vous ne souhaitez pas réinstaller le programme.
