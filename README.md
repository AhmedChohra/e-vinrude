# Avant-propos

## Organisation

Toute cette documentation est organisée en 5 grandes sections :

- [Recettes](cookbooks/)

> Des recettes pour effectuer des tâches précises, comme installer et configurer nos machines (Ubuntu), mettre en place un script de déploiement ou personnaliser la ligne de commande.

- [Composants](components/)

> Les composants sass, html et js que nous utilisons régulièrement

- [Snippets / Extraits de code](snippets/)

> Des extraits de code utiles au quotidien

- [Traits](traits/)

> Des traits PHP à intégrer dans divers projets, selon les besoins

- [Veille](watch/)

> La veille de Drakona, rangée et taguée pour la retrouver plus facilement

## Comment contribuer

- Cloner le projet
- Créer une nouvelle branche
- Créer les fichiers / dossiers nécessaires dans votre doc
- Référencer les liens vers les fichiers dans le menu, dans `.vuepress/config.js`
- Une fois terminé, [créer une Merge Request (MR) sur le gitlab du projet](https://gitlab.drakona.fr/drakona/interne/evinrude/merge_requests/new)
- Lorsque cette MR est acceptée, le site est automatiquement mis à jour (sauf erreur de compilation ou de déploiement)
- *Et voilà !*

Par défaut, les fichiers `README.md` servent d'index d'un dossier (équivalent d'un fichier `ìndex.html`, en somme) et seuls les fichiers `.md` sont affichable sur le site.

Pour compiler la doc en temps réel (mode dev, par défaut, disponible sur [http://localhost:8080](http://localhost:8080)) :

```bash
npm run docs:dev
```

Pour le mode prod :

```bash
npm run docs:build
```

### Exemple

Je crée un composant `button`, contenant un fichier expliquant le javascript, un pour le sass, un pour le html et un dernier pour envoyer vers ces docs.

L'arborescence créée :

```
components
| button
| | html.md
| | js.md
| | README.md
| | sass.md
```

Les fichiers sont ensuite ajoutés automatiquement dans le menu.
