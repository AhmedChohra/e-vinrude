const glob = require('glob');

let componentsFiles = glob.sync('components/**/*.md').map(f => '/' + f.split('README.md')[0]);
let snippetsDevFiles = glob.sync('snippets/dev/**/*.md').map(f => '/' + f.split('README.md')[0]);
let snippetsAdminFiles = glob.sync('snippets/adminSys/**/*.md').map(f => '/' + f.split('README.md')[0]);
let snippetsFrontFiles = glob.sync('snippets/front/**/*.md').map(f => '/' + f.split('README.md')[0]);
let traitsEntityFiles = glob.sync('traits/entity/**/*.md').map(f => '/' + f.split('README.md')[0]);
let traitsRepoFiles = glob.sync('traits/repository/**/*.md').map(f => '/' + f.split('README.md')[0]);
let traitsServiceFiles = glob.sync('traits/service/**/*.md').map(f => '/' + f.split('README.md')[0]);
let watchFiles = glob.sync('watch/**/*.md').map(f => '/' + f.split('README.md')[0]);
let cookbooksFiles = glob.sync('cookbooks/**/*.md').map(f => '/' + f.split('README.md')[0]);

module.exports = {
  title: 'E-vinrude',
  description: 'La doc, la veille et les composants (html, sass, js) de Drakona',
  searchPlaceholder: 'Chercher...',
  lastUpdated: 'Dernière mise à jour ',
  themeConfig: {
    nav: [
      { text: 'Accueil', link: '/' },
      { text: 'Composants', link: '/components/' },
      { text: 'Snippets', link: '/snippets/' },
      { text: 'Traits', link: '/traits/' },
      { text: 'Veille', link: '/watch/' },
      { text: 'CookBooks', link: '/cookbooks/' }
    ],
    smoothScroll: true,
    sidebar: [
      '/',
      {
        title: 'Composants',
        collapsable: true,
        sidebarDepth: 5,
        path: '/components/',
        children: componentsFiles
      },
      {
        title: 'Snippets',
        collapsable: true,
        sidebarDepth: 4,
        path: '/snippets/',
        children: [
          {
            title: 'Quotidien du développeur',
            children: snippetsDevFiles
          },
          {
            title: 'Administration Système',
            children: snippetsAdminFiles
          },
          {
            title: 'Front',
            children: snippetsFrontFiles
          }
        ]
      },
      {
        title: 'Traits',
        collapsable: true,
        sidebarDepth: 4,
        path: '/traits/',
        children: [
          {
            title: 'Entités',
            children: traitsEntityFiles
          },
          {
            title: 'Repositories',
            children: traitsRepoFiles
          },
          {
            title: 'Services',
            children: traitsServiceFiles
          }
        ]
      },
      {
        title: 'Veille',
        collapsable: true,
        sidebarDepth: 4,
        path: '/watch/',
        children: watchFiles
      },
      {
        title: 'CookBooks',
        collapsable: true,
        sidebarDepth: 4,
        path: '/cookbooks/',
        children: cookbooksFiles
      }
    ]
  },
  plugins: {
    'robots': {
      /**
       * @host
       * Mandatory, You have to provide the host URL
       */
      host: "https://e-vinrude.drakona.fr",
      /**
       * @disallowAll
       * Optional: if it's true, all others options are ignored and exclude all robots from the entire server
       */
      disallowAll: true,
      /**
       * @allowAll
       * Optional: if it's true and @disallowAll is false, all others options are ignored and allow all robots complete access
       */
      allowAll: false
    }
  }
}
